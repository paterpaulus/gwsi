# GWSI-Aufgaben nach Thema

## Wahrscheinlichkeiten

- [ ] WS12/Aufgabe 2 (7 P.)

## Stichproben

- [x] SS11/Aufgabe 1 (10 P.)
- [x] WS11/Aufgabe 1 (10 P.)
- [x] SS12/Aufgabe 1 (10 P.)
- [x] WS12/Aufgabe 1 (10 P.)
- [x] SS13/Aufgabe 1 (10 P.)
- [x] WS13/Aufgabe 1 (10 P.)

## Zufallsvariablen

- [x] SS11/Aufgabe 2 (10 P.): Gleichverteilung, Dichtefunktion
- [x] SS11/Aufgabe 3 (10 P.): Bin, Poisson
- [ ] SS11/Aufgabe 4 (10 P.): Tabelle, E, V, 2. Moment
- [ ] WS11/Aufgabe 2 (10 P.)
- [ ] SS12/Aufgabe 2 (12 P.)
- [ ] SS12/Aufgabe 4 (10 P.)
- [ ] WS12/Aufgabe 4 (10 P.)
- [ ] WS12/Aufgabe 5 (13 P.)
- [ ] WS13/Aufgabe 2 (10 P.)
- [x] WS16/Aufgabe 3 (10 P.): Zufallsvariablen, Normalverteilung
- [x] WS18/Aufgabe 2 (10 P.): Tabelle, E, V, Kovarianz

## Kombinatorik

- [x] SS11/Aufgabe 3 (10 P.)
- [ ] SS12/Aufgabe 3 (9 P.)

## Verteilungen

- [ ] SS11/Aufgabe 3 (10 P.)
- [ ] WS11/Aufgabe 3 (10 P.)
- [ ] WS11/Aufgabe 4 (10 P.)
- [ ] WS12/Aufgabe 3 (14 P.)
- [ ] SS13/Aufgabe 3 (10 P.)
- [x] WS18/Aufgabe 3 (10 P.): Exp, Erwartungswert, Varianz, Momentenschtzer
- [x] WS18/Aufgabe 4 (10 P.): Normalverteilung, E, V, Kovarianz, PHI

## Sch�tzer

- [ ] SS11/Aufgabe 5 (10 P.)
- [ ] WS11/Aufgabe 5 (10 P.)
- [ ] SS12/Aufgabe 5 (9 P.)
- [ ] SS13/Aufgabe 4 (10 P.)
- [ ] SS13/Aufgabe 4 (10 P.)
- [ ] SS17/Aufgabe 5 (10 P.)
- [ ] WS17/Aufgabe 5 (10 P.)
- [ ] SS18/Aufgabe 5 (10 P.)